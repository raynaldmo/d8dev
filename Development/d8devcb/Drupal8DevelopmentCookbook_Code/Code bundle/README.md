Drupal 8 Development Cookbook
====================================

This contains all of the code found within the chapters of the book. The code is
grouped by the chapter is belongs to. The code will either contain a Drupal
module or theme.

Chapter 4 and Chapter 5 have the code from each chapter in its respective
extension. Chapters 6, 7, 10, 11, 13 have the code for each recipe in its own
folder and the combined code from the chapter overall.

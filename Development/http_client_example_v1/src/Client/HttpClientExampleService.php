<?php

namespace Drupal\http_client_example\Client;
use GuzzleHttp\Client;

/**
 * Class HttpClientExampleService.
 *
 * @package Drupal\http_client_example
 */
class HttpClientExampleService  {

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;
  /**
   * Constructor.
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }


  public function isAlive() {
    $client = $this->httpClient;
    $request = $client->get('https://www.drupal.org');
    return $request->getStatusCode();
  }

}

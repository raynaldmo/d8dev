<?php

namespace Drupal\http_client_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\http_client_example\Client\HttpClientExampleService;
use Drupal\http_client_example\Client\HttpClientExampleServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class HttpClientExampleController.
 *
 * @package Drupal\http_client_example\Controller
 */
class HttpClientExampleController extends ControllerBase {

  private $client;

  /**
   * HttpClientExampleController constructor.
   *
   * @param $client
   */
  public function __construct( HttpClientExampleService $client) {
    $this->client = $client;
  }

  /**
   * Hello.
   *
   * @return string
   *   Return Hello string.
   */
  public function hello($name) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: hello with parameter(s): $name'),
    ];
  }

  public static function create(ContainerInterface $container) {
    $client = $container->get('http_client_example.example_service');
    return new static($client);
  }

  public function test() {
    $result = $this->client->isAlive();

    return [
      '#type' => 'markup',
      '#markup' => $result,
    ];
  }


}

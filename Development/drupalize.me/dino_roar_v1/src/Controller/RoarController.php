<?php

namespace Drupal\dino_roar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dino_roar\Jurassic\RoarGenerator;

// use Symfony\Component\HttpFoundation\Response;

/**
 * Class RoarController.
 *
 * @package Drupal\dino_roar\Controller
 */
class RoarController extends ControllerBase {

  /**
   * Roar
   *
   * @param $count
   * @return array
   */
  public function roar($count) {
    $roarGenerator = new RoarGenerator();

    $output = $roarGenerator->getRoar($count);

    return [
      '#type' => 'markup',
      '#markup' => $output,
    ];
  }

}

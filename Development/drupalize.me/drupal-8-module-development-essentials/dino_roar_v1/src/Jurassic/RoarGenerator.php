<?php

namespace Drupal\dino_roar\Jurassic;

/**
 * Class RoarGenerator.
 *
 * @package Drupal\dino_roar
 */
class RoarGenerator {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  public function getRoar($length) {
    return 'R' . str_repeat('O', $length) . 'AR';
  }
}

<?php

namespace Drupal\logger_example;
use Psr\Log\LoggerInterface;

/**
 * Class LoggerExampleService.
 *
 * @package Drupal\logger_example
 */
class LoggerExampleService implements LoggerInterface {

  /**
   * Constructor.
   */
  public function __construct() {
    // drupal_set_message('Logger example constructor');
  }

  public function log($level, $message, array $context = array()) {
    // TODO: Implement log() method.
  }

  public function emergency($message, array $context = array()) {
    // TODO: Implement emergency() method.
  }

  public function alert($message, array $context = array()) {
    // TODO: Implement alert() method.
  }

  public function critical($message, array $context = array()) {
    // TODO: Implement critical() method.
  }

  public function error($message, array $context = array()) {
    // TODO: Implement error() method.
  }

  public function warning($message, array $context = array()) {
    // TODO: Implement warning() method.
  }

  public function notice($message, array $context = array()) {
    drupal_set_message('Logger notice called');
  }

  public function info($message, array $context = array()) {
    // TODO: Implement info() method.
  }

  public function debug($message, array $context = array()) {
    // TODO: Implement debug() method.
  }
}

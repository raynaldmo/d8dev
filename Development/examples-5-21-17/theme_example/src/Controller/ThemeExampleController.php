<?php

namespace Drupal\theme_example\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ThemeExampleController.
 *
 * @package Drupal\theme_example\Controller
 */
class ThemeExampleController extends ControllerBase {
  /**
   * @return array
   */
  public function simple() {
    return [
      'example one' => [
        '#markup' => '<div>Markup Example</div>',
      ],
      'example two' => [
        '#type' => 'my_element',
        '#label' => $this->t('Example Label'),
        '#description' => $this->t('This is the description text.'),
      ],
    ];
  }

}

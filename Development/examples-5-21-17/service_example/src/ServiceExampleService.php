<?php

namespace Drupal\service_example;

/**
 * Class ServiceExampleService.
 *
 * @package Drupal\service_example
 */
class ServiceExampleService {

  protected $service_example_value;

  /**
   * ServiceExampleService constructor.
   *
   * When the service is created, set a value for the example variable
   */
  public function __construct() {
    $this->service_example_value = 'Student';
  }

  /**
   * Return the value of the example variable
   *
   * @return string
   */
  public function getServiceExampleValue(): string {
    return $this->service_example_value;
  }

}

<?php

namespace Drupal\service_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\service_example\ServiceExampleService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ServiceExampleController.
 *
 * @package Drupal\service_example\Controller
 *
 *
 */
class ServiceExampleController extends ControllerBase {

  /**
   * @var \Drupal\service_example\ServiceExampleService
   */
  protected $serviceExampleService;

  /**
   * {@inheritdoc}
   */
  public function __construct(ServiceExampleService $serviceExampleService) {
    drupal_set_message('ServiceExampleController constructor');
   $this->serviceExampleService = $serviceExampleService;

  }

  /**
   * {@inheritdoc}
   *
   * create() call is inherited from ControllerBase
   */
  public static function create(ContainerInterface $container) {
    // Late static binding in the house :)
    // The below is essentially the same as doing:
    // new ServiceExampleController(ServiceExampleService $service)
    $serviceExampleService = new static($container->get('service_example.example_service'));

    return $serviceExampleService;

  }

  public function simple_example() {

    $output = [
      '#type' => 'markup',
      '#markup' => $this->serviceExampleService->getServiceExampleValue(),
    ];

    return $output;
  }

}

## Creating a Custom Filter in Drupal 8
* 8/27/17
* See https://www.lullabot.com/articles/creating-a-custom-filter-in-drupal-8 
* Source is available at https://github.com/jazzdrive3/celebrate-filter-example
* Steps
  * Download and unzip source from above location into modules/custom/miscellaneous
  * Rename module from celebrate-filter-example to celebrate
  * Check that namespace declaration in FilterCelebrate.php is correct
    * If you don't do above you'll get error
    ```php
    Drupal\Component\Plugin\Exception\PluginException: Plugin (filter_celebrate) instance class "Drupal\celebrate\Plugin\Filter\FilterCelebrate" does not exist. in Drupal\Component\Plugin\Fact
    ```
* Tested by creating test Article and it works!
<?php

namespace Drupal\sample_page_with_service\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;

/**
 * Class SamplePageController.
 *
 * @package Drupal\sample_page_with_service\Controller
 */
class SamplePageController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigManager $config_manager) {
    $this->configManager = $config_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.manager')
    );
  }

  /**
   * Page.
   *
   * @return array
   *   Return render array
   */
  public function page($name) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Sample page with service: Hello @name',
        ['@name' => $name]),
    ];
  }

}

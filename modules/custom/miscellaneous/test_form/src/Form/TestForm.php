<?php

namespace Drupal\test_form\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\State;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class TestForm.
 *
 * @package Drupal\test_form\Form
 */
class TestForm extends ConfigFormBase {

  private $state;

  public function __construct(ConfigFactoryInterface $config_factory, State $state) {
    parent::__construct($config_factory);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['test_form.settings'];
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#placeholder' => 'Enter Name',
      '#description' => $this->t('Name'),
    ];

    $form['phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone Number'),
      '#placeholder' => 'Enter Phone Number',
      '#description' => $this->t('Phone Number'),
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#placeholder' => 'Enter Password',
      '#description' => $this->t('Password'),
    ];

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate name
    $name = $form_state->getValue('name');
    if (empty($name)) {
      $form_state->setErrorByName('name', t('You must enter a name.'));
    }

    // Validate password
    $password = $form_state->getValue('password');
    if (!empty($password)) {
      if (strlen($password) < 8) {
        $form_state->setErrorByName('password', t('Password must be at least 8 characters long.'));
      }
      if (!preg_match('/[A-Z]/', $password)) {
        $form_state->setErrorByName('password', t('Password must contain at least one uppercase character.'));
      }
    } else {
      $form_state->setErrorByName('password', t('You must enter a password.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save values in state store
    $this->state->set('test_form_name', $form_state->getValue('name'));
    $this->state->set('test_form_phone', $form_state->getValue('phone'));
    $this->state->set('test_form_password', $form_state->getValue('password'));

    drupal_set_message($this->t('%msg', ['%msg' => 'Saved data to state store.']));
  }

}

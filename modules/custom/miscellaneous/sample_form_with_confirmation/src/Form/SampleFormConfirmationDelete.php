<?php

declare (strict_types = 1);

namespace Drupal\sample_form_with_confirmation\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class SampleFormConfirmationDelete.
 *
 * @package Drupal\sample_form_with_confirmation\Form
 */
class SampleFormConfirmationDelete extends ConfirmFormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sample_form_confirmation_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return parent::buildForm($form, $form_state);
  }

  public function getQuestion() {
    return $this->t('Are you sure you want to delete?');

  }

  public function getCancelUrl() {
    return new Url(
      'sample_form_with_confirmation.sample_form_with_confirmation'
    );
  }

  /**
    * {@inheritdoc}
    */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
        // drupal_set_message($key . ': ' . $value);
    }

    $config = \Drupal::service('config.factory')->getEditable(
      'sample_form_with_confirmation.settings'
    );
    $config->delete();

    drupal_set_message($this->t('Deleted user.'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}

<?php

namespace Drupal\sample_form_with_confirmation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SampleFormWithConfirmation.
 *
 * @package Drupal\sample_form_with_confirmation\Form
 */
class SampleFormWithConfirmation extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sample_form_with_confirmation.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sample_form_with_confirmation';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#options' => [
        'create' => $this->t('Create'),
        'delete' => $this->t('Delete'),
      ],
      '#empty_option' => $this->t('-select-'),
      '#description' => $this->t('Operation'),
    ];

    $config = $this->config('sample_form_with_confirmation.settings');

    // Build data to display in table
    $rows = [];
    $row = [];
    $row[] = $config->get('username');
    $row[] = $config->get('email');
    $rows[] = $row;

    $header = [$this->t('User name'), $this->t('Email')];

    $form['user_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No users'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    foreach ($form_state->getValues() as $key => $value) {
      // $message = $key . ': ' . $value;
      // drupal_set_message($this->t('@message', ['@message' => $message]));
    }

    $operation = ($form_state->getValue('operation'));

    if ($operation == 'create') {
      $config = $this->config('sample_form_with_confirmation.settings');
      $config->set('username', 'Mickey Mouse');
      $config->set('email', 'mickey@disney.com');
      $config->set('password', 'password');
      $config->save();

      drupal_set_message($this->t('Created user.'));
    }
    elseif ($operation == 'delete') {
      $form_state->setRedirect(
        'sample_form_with_confirmation.sample_form_confirmation_delete'
      );
    }
    else {
      drupal_set_message($this->t('No operation selected.'));
    }
  }

}

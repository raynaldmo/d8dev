<?php

namespace Drupal\recipe\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Recipe entities.
 *
 * @ingroup recipe
 */
class RecipeDeleteForm extends ContentEntityDeleteForm {


}

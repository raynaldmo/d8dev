### Recipe Content Entity

10-27-17

* Module demonstrating the implementation of a custom content entity.  
* Create a content entity type called "Recipe" using drupal console 
* Modify drupal console 'boilerplate' code to change menu links etc.
* Use examples/content_entity_example contrib module as guide in implementing

#### Generate module and Entity boilerplate code
```
$drupal gm
$drupal geco
```
* drupal geco generates:  
src/ files  
templates/ files  
recipe.links.action.yml  
recipe.links.menu.yml  
recipe.links.task.yml  
recipe.links.permissions.yml  
recipe.page.inc  

* Enable recipe module
* These links/pages will be created
  * Structure -> Recipe settings -> Recipe (Entity) settings page
  * Structure -> Recipe list -> Recipe entities page

* Default DB table

```
mysql> describe recipe;
+----------+------------------+------+-----+---------+----------------+
| Field    | Type             | Null | Key | Default | Extra          |
+----------+------------------+------+-----+---------+----------------+
| id       | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
| uuid     | varchar(128)     | NO   | UNI | NULL    |                |
| langcode | varchar(12)      | NO   |     | NULL    |                |
| user_id  | int(10) unsigned | NO   | MUL | NULL    |                |
| name     | varchar(50)      | YES  |     | NULL    |                |
| status   | tinyint(4)       | NO   |     | NULL    |                |
| created  | int(11)          | YES  |     | NULL    |                |
| changed  | int(11)          | YES  |     | NULL    |                |
+----------+------------------+------+-----+---------+----------------+
8 rows in set (0.00 sec)
```

#### Change menu links
* Create recipe.routing.yml file
* Add the following routes
  * entity.recipe.canonical
  * entity.recipe.collection
  * entity.recipe.add_form
  * entity.recipe.edit_form
  * entity.recipe.delete_form

* Modify links keys in Recipe.php annotation to match above routing entries
* Updated recipe.menu.link.yml to place Recipe List page under /admin/content
* Updated RecipeListBuilder::buildRow() so link points to entity instead of entity edit form
* Added 'edit' operation in RecipeAccessControlHandler::checkAccess

#### Add additional field(s)
* Fields are defined by Drupal\Core\Field\BaseFieldDefinition
* Choose field type
```
# get list of field types
drupal dpl field.field_type
```

Category (Select list; Breakfast, Lunch etc.)
Recipe Name (string)
Ingredients (array of strings)
Cooking Instructions (textarea)

#### Add custom field(s)

<?php

namespace Drupal\sample_content_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Sample ce entities.
 *
 * @ingroup sample_content_entity
 */
class SampleCEDeleteForm extends ContentEntityDeleteForm {


}

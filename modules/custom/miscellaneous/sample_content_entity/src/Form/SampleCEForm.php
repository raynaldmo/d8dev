<?php

namespace Drupal\sample_content_entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Sample ce edit forms.
 *
 * @ingroup sample_content_entity
 */
class SampleCEForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\sample_content_entity\Entity\SampleCE */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Sample ce.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Sample ce.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.sample_c_e.canonical', ['sample_c_e' => $entity->id()]);
  }

}

<?php

namespace Drupal\sample_content_entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Sample ce entity.
 *
 * @see \Drupal\sample_content_entity\Entity\SampleCE.
 */
class SampleCEAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\sample_content_entity\Entity\SampleCEInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished sample ce entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published sample ce entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit sample ce entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete sample ce entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add sample ce entities');
  }

}

<?php

namespace Drupal\sample_content_entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Sample ce entities.
 *
 * @ingroup sample_content_entity
 */
class SampleCEListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Sample ce ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\sample_content_entity\Entity\SampleCE */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.sample_c_e.edit_form',
      ['sample_c_e' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}

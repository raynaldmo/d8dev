<?php

namespace Drupal\sample_content_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Sample ce entities.
 *
 * @ingroup sample_content_entity
 */
interface SampleCEInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /*
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Sample ce name.
   *
   * @return string
   *   Name of the Sample ce.
   */
  public function getName();

  /**
   * Sets the Sample ce name.
   *
   * @param string $name
   *   The Sample ce name.
   *
   * @return \Drupal\sample_content_entity\Entity\SampleCEInterface
   *   The called Sample ce entity.
   */
  public function setName($name);

  /**
   * Gets the Sample ce creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Sample ce.
   */
  public function getCreatedTime();

  /**
   * Sets the Sample ce creation timestamp.
   *
   * @param int $timestamp
   *   The Sample ce creation timestamp.
   *
   * @return \Drupal\sample_content_entity\Entity\SampleCEInterface
   *   The called Sample ce entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Sample ce published status indicator.
   *
   * Unpublished Sample ce are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Sample ce is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Sample ce.
   *
   * @param bool $published
   *   TRUE to set this Sample ce to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\sample_content_entity\Entity\SampleCEInterface
   *   The called Sample ce entity.
   */
  public function setPublished($published);

}

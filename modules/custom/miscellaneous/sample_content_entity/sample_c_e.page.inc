<?php

/**
 * @file
 * Contains sample_c_e.page.inc.
 *
 * Page callback for Sample ce entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Sample ce templates.
 *
 * Default template: sample_c_e.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_sample_c_e(array &$variables) {
  // Fetch SampleCE Entity Object.
  $sample_c_e = $variables['elements']['#sample_c_e'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

<?php

namespace Drupal\sample_redirect\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Url;

/**
 * Class SampleEventsSubscriber.
 *
 * @package Drupal\sample_redirect
 */
class SampleEventsSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['sampleRedirect'];
    return $events;
  }

  /**
   * This method is called whenever the kernel.request event is dispatched.
   *
   * @param GetResponseEvent $event
   *   Event object.
   */
  public function sampleRedirect(GetResponseEvent $event) {

    $request = $event->getRequest();
    $request_path = $request->getPathInfo();

    if ($request_path !== '/redirect/me') {
      return;
    }

    $redirect_url = Url::fromUri('/');
    $response = new RedirectResponse($redirect_url->toString(), 301);
    $event->setResponse($response);
    drupal_set_message(t('Event kernel.request thrown by Subscriber in module sample_redirect.'), 'status', TRUE);
  }

}

<?php

namespace Drupal\http_client_example\Client;

/**
 * Interface HttpClientExampleServiceInterface.
 *
 * @package Drupal\http_client_example
 */
interface HttpClientExampleServiceInterface {
  /**
   * Utilizes Drupal's httpClient to 'ping' a website
   *
   * @param string $url
   *  website url
   * @param string $method
   *   get, post, patch, delete, etc. See Guzzle documentation.
   * @param string $endpoint
   *   The API endpoint (ex. nba/players)
   * @param array $query
   *   Query string parameters the endpoint allows (ex. ['per_page' => 50]
   * @param array $body (converted to JSON)
   *   Utilized for some endpoints
   * @return object
   *   \GuzzleHttp\Psr7\Response body
   */
  public function connect($url, $method, $endpoint, $query, $body);

}

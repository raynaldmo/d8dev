<?php

namespace Drupal\http_client_example\Client;

use GuzzleHttp\Client;
use \GuzzleHttp\Exception\RequestException;

/**
 * Class HttpClientExampleService.
 *
 * @package Drupal\http_client_example
 */
class HttpClientExampleService implements HttpClientExampleServiceInterface {

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructor.
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * { @inheritdoc }
   */
  public function connect($url, $method, $endpoint, $query, $body) {
    $options = $this->buildOptions($query, $body);

    try {
      $response = $this->httpClient->{$method}($url . $endpoint, $options);

    } catch (RequestException $exception) {
      drupal_set_message(t('Failed to connect to @site : "%error"',
        ['@site' => $url, '%error' => $exception->getMessage()]), 'error');

      \Drupal::logger('http_client_example')->error(
        'Failed to connect to @site :  "%error"',
        ['@site' => $url, '%error' => $exception->getMessage()]
      );
      return FALSE;
    }

    return $response;
  }

  /**
   * Build options for the client.
   */
  private function buildOptions($query, $body) {
    $options = [];
    if ($body) {
      $options['body'] = $body;
    }
    if ($query) {
      $options['query'] = $query;
    }
    return $options;
  }

  /**
   * @param $url
   *
   * @return bool
   */
  public function isAlive($url) {
    $response = $this->connect($url, 'get', '/', [], []);
    if (is_object($response)) {
      return $response->getStatusCode() == 200 ? TRUE : FALSE;
    }
    return FALSE;
  }

}

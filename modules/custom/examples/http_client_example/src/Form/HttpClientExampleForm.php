<?php

namespace Drupal\http_client_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\http_client_example\Client\HttpClientExampleService;

/**
 * Class HttpClientExampleForm.
 *
 * @package Drupal\http_client_example\Form
 */
class HttpClientExampleForm extends FormBase {

  /**
   * @var \Drupal\http_client_example\Client\HttpClientExampleServiceInterface
   */
  protected $client;

  /**
   * HttpClientExampleForm constructor.
   *
   * @param \Drupal\http_client_example\Client\HttpClientExampleService $http_client
   */
  public function __construct(HttpClientExampleService $http_example_client) {
    $this->client = $http_example_client;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client_example.example_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'http_client_example_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['website'] = [
      '#type' => 'url',
      '#title' => $this->t('Enter Website url'),
      '#description' => $this->t('Website to test'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $website = $form_state->getValue('website');
    if ($this->client->isAlive($website)) {
      drupal_set_message(t('@site is UP', ['@site' => $website]));
    } else {
      drupal_set_message(t('@site is DOWN', ['@site' => $website]), 'error');
    }
  }

}

<?php

namespace Drupal\sandwich\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Sandwich plugin plugins.
 */
abstract class SandwichPluginBase extends PluginBase implements SandwichPluginInterface {

  // Add common methods and abstract methods for your plugin type here.

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->getPluginDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function getCalories() {
    return 100.00;
  }

  /**
   * {@inheritdoc}
   */
  abstract function order(array $extras);
}

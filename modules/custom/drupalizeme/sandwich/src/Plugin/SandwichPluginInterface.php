<?php

namespace Drupal\sandwich\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Sandwich plugin plugins.
 */
interface SandwichPluginInterface extends PluginInspectionInterface {
  // Add get/set methods for your plugin type here.

  /**
   * Provide a description of the sandwich
   *
   * @return string
   *  A string description of the sandwich
   */
  public function getDescription();

  /**
   * Provide the number of calories per serving for the sandwich
   *
   * @return float
   *  The number of calories per serving
   */
  public function getCalories();

  /**
   * Place and order for a sandwich
   *
   * @param array $extras
   *  An array of extra ingredients to include with the sandwich
   *
   * @return string
   *  Description of the sandwich that was just ordered
   */
  public function order(array $extras);

}

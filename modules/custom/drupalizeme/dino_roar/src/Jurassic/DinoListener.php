<?php

/**
 *  Example implementation of an event listener
 */

namespace Drupal\dino_roar\Jurassic;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class  DinoListener implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerFactory;

  /**
   * DinoListener constructor.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   */
  public function __construct(LoggerChannelFactoryInterface $loggerFactory) {

    $this->loggerFactory = $loggerFactory;
  }

  /**
   * onKernelRequest event listener
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   * $event type will change depending on which event is being listened to
   */
  public function onKernelRequest(GetResponseEvent $event) {
    $request = $event->getRequest();

    // check if 'roar' is in query string i.e. /path/?roar=xxx
    // this
    $shouldRoar = $request->query->get('roar');
    if ($shouldRoar) {
      drupal_set_message('ROOOOOOOOAR');
      // log message to dblog
      $this->loggerFactory->get('dino_roar')->notice('Roar requested ROOOOOOAR');

      // This works !
      \Drupal::logger('dino_roar')->notice('Message from global logger');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
     KernelEvents::REQUEST => 'onKernelRequest'
    ];
  }

}
<?php
/**
 * Example implementation of a service
 *
 */
namespace Drupal\dino_roar\Jurassic;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 * RoarGenerator service
 *
 * @package Drupal\dino_roar
 */
class RoarGenerator {
  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  private $keyValueFactory;

  // Flag to use/not use key value store
  private $useCache;

  /**
   * RoarGenerator constructor.
   *
   * Note the constructor parameters are provided in dino_roar.services.yml
   * file !
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   * @param $useCache
   */
  public function __construct(KeyValueFactoryInterface $keyValueFactory , $useCache)  {
    // This is for the keyvalue service
    $this->keyValueFactory = $keyValueFactory;
    $this->useCache = $useCache;
  }

  /**
   * Return 'roar' string
   *
   * @param $length
   * @return mixed|string
   */
  public function getRoar($length) {
    // Example of caching the result of an operation. Here we cache the
    // the string generated with the assumption that it's computationally
    // expensive.
    // Name the store and the key
    $key = 'roar_' . $length;
    $store = $this->keyValueFactory->get('dino');

    if ($this->useCache && $store->has($key)) {
      return $store->get($key);
    }

    // sleep(10);

    $string =  'R' . str_repeat('O', $length) . 'AR';
    if ($this->useCache) {
      $store->set($key, $string);
    }

    return $string;
  }
}

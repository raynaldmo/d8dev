<?php

namespace Drupal\dino_roar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\dino_roar\Jurassic\RoarGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

// use Symfony\Component\HttpFoundation\Response;

/**
 * Class RoarController.
 *
 * @package Drupal\dino_roar\Controller
 *
 * Controllers do not necessarily have to extend ControllerBase but in
 * doing so it gives controller lots of useful methods.
 *
 * See ControllerBase.php
 */
class RoarController extends ControllerBase {

  private $roarGenerator;

  /**
   * @var LoggerChannelFactoryInterface
   */
  private $loggerFactoryService;

  /**
   * {@inheritdoc}
   */
  public function __construct(RoarGenerator $roarGenerator, LoggerChannelFactoryInterface $loggerFactoryService) {
    $this->roarGenerator = $roarGenerator;
    $this->loggerFactoryService = $loggerFactoryService;
  }

  /**
   * Roar
   *
   * @param $count
   * @return array
   */
  public function roar($count) {

    $output = $this->roarGenerator->getRoar($count);

    // Note: May be able to use LoggerChannelTrait::getLogger() instead of
    // logger.factory service
    if (!empty($this->loggerFactoryService)) {
      $this->loggerFactoryService->get('default')->debug($output);
    }

    return [
      '#type' => 'markup',
      '#markup' => $output,
    ];

  }

  /**
   * {@inheritdoc}
   *
   * Define and instantiate services needed by RoarController
   *
   * create() call is inherited from ControllerBase - gives the controller
   * ability
   */
  public static function create(ContainerInterface $container) {
    $roarGenerator = $container->get('dino_roar.roar_generator');
    $loggerFactory = $container->get('logger.factory');

    // Once the create() object is added, the controller code is responsible for
    // creating a new controler object.
    // The below creates a new RoarController object with the advantage that
    // we have the flexibility of passing in any service we get from the
    // service container for use in our controller.
    // Any needed configuration can also be passed in this way
    return new static($roarGenerator, $loggerFactory);
  }

}

<?php

namespace Drupal\hello_world\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\hello_world\HelloWorldSalutation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class HelloWorldController.
 */
class HelloWorldController extends ControllerBase {


  /**
   * @var \Drupal\hello_world\HelloWorldSalutation
   */
  protected $salutation;


  /**
   * HelloWorldController constructor.
   *
   * @param \Drupal\hello_world\HelloWorldSalutation $salutation
   */
  public function __construct(HelloWorldSalutation $salutation) {
    $this->salutation = $salutation;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hello_world.salutation')
    );
  }

  /**
   * Get and return Salutation message.
   *
   * @return array
   *   Return page markup as render array
   */
  public function helloWorld() {
/*
    return [
      '#type' => 'markup',

      // Get Salutation message from  hello_world.salutation service.
      '#markup' => $this->salutation->getSalutation(),
    ];
*/
    return $this->salutation->getSalutationComponent();
  }

  public function hideBlock(Request $request) {
    if (!$request->isXmlHttpRequest()) {
      throw new NotFoundHttpException();
    }

    $response = new AjaxResponse();

    // RemoveComand() maps to jQuery() remove() method.
    $command = new RemoveCommand('.block-hello-world');
    $response->addCommand($command);
    return $response;
  }

}

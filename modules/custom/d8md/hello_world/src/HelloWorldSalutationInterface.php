<?php

declare (strict_types = 1);

namespace Drupal\hello_world;

/**
 * Interface HelloWorldSalutationInterface.
 *
 * Prepare a salutation to the world.
 */
interface HelloWorldSalutationInterface {

  /**
   * Return the salutation.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Salutation message.
   */
  public function getSalutation();

  /**
   * Returns the Salutation render array.
   */
  public function getSalutationComponent() : array;

}

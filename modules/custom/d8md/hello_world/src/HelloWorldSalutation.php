<?php

namespace Drupal\hello_world;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class HelloWorldSalutation.
 */
class HelloWorldSalutation implements HelloWorldSalutationInterface {

  use StringTranslationTrait;

  /**
   * Config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Event Dispatcher object.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * HelloWorldSalutation constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory object.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event Dispatcher object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher) {
    $this->configFactory = $config_factory;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function getSalutation() {
    $config = $this->configFactory->get('hello_world.custom_salutation');
    $salutation = $config->get('salutation');

    if ($salutation != "") {
      // If we're not using event dispatcher, just return configured
      // salutation.
      // return $salutation
      $event = new SalutationEvent();
      // Set configured salutation.
      $event->setValue($salutation);

      // Inform event subscribers of salutation message.
      // Subscribers should listen to SalutationEvent:EVENT event.
      $event = $this->eventDispatcher->dispatch(SalutationEvent::EVENT, $event);
      return $event->getValue();
    }

    // No salutation message has been configured. Build a default one.
    $time = new \DateTime();

    $format = 'g:i a, l, F j, Y';
    // drupal_set_message($time->getTimezone()->getName());
    if ((int) $time->format('G') >= 06 && (int) $time->format('G') < 12) {
      return $this->t('Good morning world. The time is %time',
        ['%time' => $time->format($format)]);
    }

    if ((int) $time->format('G') >= 12 && (int) $time->format('G') < 18) {
      return $this->t('Good afternoon world. The time is %time',
        ['%time' => $time->format($format)]);
    }

    if ((int) $time->format('G') >= 18) {
      return $this->t('Good evening world. The time is %time',
        ['%time' => $time->format($format)]);
    }

  }

  /**
   * Returns the Salutation render array.
   */
  public function getSalutationComponent() : array {

    // This render array is the 'connection' to hello-world-salutation.html.twig
    // TWIG template file.
    $render = [
      '#theme' => 'hello_world_salutation',
    ];

    $config = $this->configFactory->get('hello_world.custom_salutation');
    $salutation = $config->get('salutation');

    if ($salutation != "") {
      // Note that #salutation and #overriden are not properties known to the
      // render system. They are not properties but are known by the theme
      // system by virtue of the fact we defined them as variables in
      // hello_world_hook_theme.
      $render['#salutation'] = $salutation;
      $render['#overridden'] = TRUE;
      return $render;
    }

    // No salutation message has been configured. Build a default one.
    $time = new \DateTime();
    $format = 'g:i a, l, F j, Y';
    // drupal_set_message($time->getTimezone()->getName());
    $render['#target'] = $this->t('This is the target');

    // Uses hello_world.libraries.yml file.
    $render['#attached']['library'][] = 'hello_world/hello_world_clock';

    if ((int) $time->format('G') >= 06 && (int) $time->format('G') < 12) {
      $render['#salutation'] = $this->t('Good morning. The time is %time.',
        ['%time' => $time->format($format)]);
      return $render;
    }

    if ((int) $time->format('G') >= 12 && (int) $time->format('G') < 18) {
      $render['#salutation'] = $this->t('Good afternoon. The time is %time.',
        ['%time' => $time->format($format)]);
      $render['#attached']['drupalSettings']['hello_world']['hello_world_clock']['afternoon'] = TRUE;
      return $render;
    }

    if ((int) $time->format('G') >= 18) {
      $render['#salutation'] = $this->t('Good evening. The time is %time.',
        ['%time' => $time->format($format)]);
      return $render;
    }
  }

}

<?php

declare (strict_types = 1);

namespace Drupal\products\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Importer entities.
 */
interface ImporterInterface extends ConfigEntityInterface {

  /**
   * Returns the Uri where the import can get the data from.
   *
   * @return \Drupal\Core\Url
   *   Return url.
   */
  public function getUrl();

  /**
   * Returns the importer plugin ID to be used by this importer.
   *
   * @return string
   *   Return plugin ID string.
   */
  public function getPluginId() : string;

  /**
   * Whether or not to update existing products if they have already
   * been imported.
   *
   * @return bool
   *   Return update existing flag.
   */
  public function updateExisting() : bool ;

  /**
   * Returns the source of the products.
   *
   * @return string
   *   Returns product source string.
   */
  public function getSource() : string;

}

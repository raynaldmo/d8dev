<?php

namespace Drupal\products\Admin;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\products\Entity\Importer;
/**
 * Class ProductImportForm.
 */
class ProductImportForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Constructs a new ProductImportForm object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'product_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Sample admin page to manually import products'),
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#title' => $this->t('Import'),
      '#description' => $this->t('Import products'),
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation needed for now.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    /*
    foreach ($form_state->getValues() as $key => $value) {
      drupal_set_message($key . ': ' . $value);
    }
    */


    /*
    // @var \Drupal\Core\Entity\EntityStorageInterface $storage
    $storage = \Drupal::entityTypeManager()->getStorage('importer');

    // @var \Drupal\Core\Entity\EntityInterface $config
    $config = $storage->load('my_json_product_importer');

    // @var \Drupal\products\Plugin\ImporterManager $importer
    $importer = \Drupal::service('products.importer_manager');

    $id = $config->getPluginId();
    $plugin = $importer->createInstance($id, ['config' => $config]);
    // $plugin = $importer->createInstance($id, []);

    $plugin->import();
*/
    $config = \Drupal::entityTypeManager()->getStorage('importer')->load('my_json_product_importer');
    $plugin = \Drupal::service('products.importer_manager')->createInstance($config->getPluginId(), ['config' => $config]);
    $plugin->import();

  }

}

<?php

namespace Drupal\mood_ring;

/**
 * Service that returns a 'mood'
 */
class MoodRing {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  protected $moods = [
    0 => 'Very Sad',
    1 => 'Sad',
    2 => 'So-so',
    3 => 'Happy',
    4 => 'Very Happy',
  ];

  /**
   * Returns a string that tells the user's current mood.
   *
   * @return string
   */
  public function getMood() {
    return $this->moods[rand(0,4)];
  }

}

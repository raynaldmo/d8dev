<?php


namespace Drupal\talk;

use Drupal\Core\Session\AccountProxy;
use Drupal\mood_ring\MoodRing;

/**
 * A service that provides a system for getting response message
 */
class TalkService {

  protected $responsesToMood = [
    'Very Sad' => 'I hope you feel better.',
    'Sad' => 'are you ok?',
    'So-so' => 'good morning.',
    'Happy' => 'what\'s Up?',
    'Very Happy' => 'you seem happy today!',
  ];


  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * @var \Drupal\mood_ring\MoodRing
   */
  protected $moodRing;

  /**
   * Inject two services and store them for use in service methods
   *
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   * @param \Drupal\mood_ring\MoodRing $moodRing
   */
  public function __construct(AccountProxy $currentUser, MoodRing $moodRing) {
    $this->currentUser = $currentUser;
    $this->moodRing = $moodRing;
  }

  /**
   * Returns a string that is a message to a user.
   *
   * @return string
   */
  public function getResponseToMood() {
    // We can use our services and their defined methods.
    $username = $this->currentUser->getUsername();
    $mood = $this->moodRing->getMood();

    // We build a message to return.
    return $username . ', ' . $this->responsesToMood[$mood];
  }

}

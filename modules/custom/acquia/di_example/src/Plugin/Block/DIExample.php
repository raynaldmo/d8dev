<?php

namespace Drupal\di_example\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\talk\TalkService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'DIExample' block.
 *
 * @Block(
 *  id = "di_example_conversation_mood",
 *  admin_label = @Translation("DI Example: Conversation about mood."),
 * )
 */
class DIExample extends BlockBase implements ContainerFactoryPluginInterface {

  protected $dITalk;

  /**
   * DIExampple constructor
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\talk\TalkService $talk
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, TalkService $talk) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->dITalk = $talk;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // We use the injected Talk service to get a message
    $message = $this->dITalk->getResponseToMood();

    $output = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t($message) . '</p>'
    ];

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('talk.default')
    );
  }
}

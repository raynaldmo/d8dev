<?php

namespace Drupal\di_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\talk\TalkService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DIController.
 *
 * @package Drupal\di_example\Controller
 */
class DIController extends ControllerBase {

  /**
   * @var \Drupal\talk\TalkService
   */
  protected $talk;

  /**
   * DIController constructor.
   *
   * @param \Drupal\talk\TalkService $talk
   */
  public function __construct(TalkService $talk) {
    $this->talk = $talk;
  }

  /**
   * Generate mood response
   *
   * @return string
   *
   */
  public function conversationAboutMood() {
    $output = $this->talk->getResponseToMood();

    return [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t($output) . '</p>'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('talk.default')
    );
  }
}

<?php

namespace Drupal\test_example;

/**
 * Class TestExampleConversions.
 *
 * @package Drupal\test_example
 */
class TestExampleConversions {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Convert Celsius to Fahrenheit
   *
   * @param $temp
   *
   * @return mixed
   */
  public function celsiusToFahrenheit($temp) {
    return ($temp *(9/5) + 32);
  }

  /**
   * Convert centimeter to inches.
   *
   * @param $length
   *
   * @return int
   */
  public function centimeterToInch($length) {
    return $length / 2.54;
  }

}

<?php

namespace Drupal\Tests\test_example\Unit;

use Drupal\test_example\TestExampleConversions;
use Drupal\Tests\UnitTestCase;


/**
 * Demonstrates how to write tests
 *
 * @group test_example
 */
class TestExampleConversionsTest extends UnitTestCase {

  public $conversionService;

  protected function setUp() {
    $this->conversionService = new TestExampleConversions();
  }

  /**
   * Tests celsiusToFahrenheit method
   */
  public function testOneConversion() {
    $this->assertEquals(32, $this->conversionService->celsiusToFahrenheit(0));
  }

  /**
   * Provides data for the testCentimeterToInches method
   *
   * @return array
   */
  public function providerCentimetersToInches() {
    return [
      [2.54,1],
      [254,100],
      [0,0],
      [-2.54,-1],
    ];
  }

  /**
   * Tests centimeterToInches method
   *
   * @dataProvider providerCentimetersToInches
   *
   * @param $length
   * @param $expectedValue
   */
  public function testCentimetersToInches($length, $expectedValue) {
    $this->assertEquals($expectedValue, $this->conversionService->centimeterToInch($length));
  }


}
Trails is a simple module that keeps track of the most recent pages visited
and adds a 'history' block to output the history as a list of links.

Current Maintainer: Chris Shattuck <http://drupal.org/user/166383>




<?php

/**
 * @file
 * Contains \Drupal\system\Form\TrailsSettingsFrom
 */

namespace Drupal\trails\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures Trails settings.
 */
class TrailsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trails_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['trails.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // copied from trails.admin.inc and made appropriate modifications
    $form['max_in_settings'] = array(
      '#type' => 'select',
      '#title' => $this->t('Maximum number of items to display'),
      '#options' => array_combine(range(1, 200), range(1, 200)),
      '#default_value' =>  $this->config('trails.settings')->get('max_in_settings'),
      '#description' => $this->t('This will set the maximum allowable number that can be displayed in a history block.'),
      '#required' => TRUE,
    );
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('trails.settings')
      ->set('max_in_settings', $form_state->getValue('max_in_settings'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

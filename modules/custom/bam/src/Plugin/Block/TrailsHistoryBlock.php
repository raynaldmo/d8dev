<?php

/**
 * @file
 * Contains \Drupal\trails\Plugin\Block\TrailsHistoryBlock
 */

namespace Drupal\trails\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides a 'Trails History' block.
 *
 * @Block(
 *   id = "trails_history_block",
 *   admin_label = @Translation("Trails History")
 * )
 */
class TrailsHistoryBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    $max_age = 0;
    return $max_age;
  }

  /**
   * {@inheritdoc}
   */
  // Implement and/or override BlockPluginInterface methods here
  public function build() {
    /*
    return array(
      '#type' => 'markup',
      '#markup' => $this->t("@@@Trails History@@@"),
      '#cache' => array(
        'keys' => [],
        'contexts' => [],
        'tags' => [],
        'max-age' => 0
      )
    );
    */

    // Create list of previous paths.

    // Grab the trail history
    $trail = \Drupal::state()->get('trails.history') ?: array();

    // Flip the saved array to show newest pages first.
    $reverse_trail = array_reverse($trail);

    // Grab the number of items to display
    // $num_items = variable_get('trails_block_num', '5');
    // $items = \Drupal::state()->get('trails.num_items');
    // $num_items = $items ? $items : 5;
    // shortcut for above two statements
    // $num_items = \Drupal::state()->get('trails.num_items') ?: 5;
    $num_items = $this->configuration['num_to_show'];

    // Output the latest items as a list
    $output = ''; // Initialize variable, this was added after the video was created.
    $items = [];
    for ($i = 0; $i < $num_items; $i++) {
      if ($item = $reverse_trail[$i]) {
        $ago = \Drupal::service('date.formatter')->formatInterval((REQUEST_TIME - $item['timestamp']));
        $url = Url::fromUri('internal:' . $item['path']);
        // This has been deprecated
        // $link = \Drupal::l(t($item['title']), $url);
        $item['title'] = $item['title'] ?: "(no title)";
        $link = Link::fromTextAndUrl(t($item['title']), $url);
        $render_arr = $link->toRenderable();
        $title = $render_arr['#title']->render();
        if (empty($title)) {
          $title = 'No title';
        }
        // Refactor to use TWIG template file. Build an output array instead of
        // raw HTML output.
        // $output .= '<li>' . $title . ' - ' . $ago . ' ' . t('ago') . '</li>';
        $items[] = array('title' => $title, 'ago' => $ago, );
      }
    }

    if (isset($output)) {
      $output = '
            <p>' . $this->t('Below are the last @num pages you have visited.', array('@num' => $num_items)) . '</p>
            <ul>' . $output . '</ul>
          ';
    }

    // return array ('#markup' => $output);
    return array (
      '#theme' => 'trails_list', '#num_items' => $num_items, '#items' => $items,
    );
  }

  /**
   * {@inheritdoc}
   *
   * Creates a generic configuration form for all block types. Individual
   * block plugins can add elements to this form by overriding
   * BlockBase::blockForm(). Most block plugins should not override this
   * method unless they need to alter the generic form elements.
   *
   * @see \Drupal\Core\Block\BlockBase::blockForm()
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Get the maximum allowed value from the configuration form.

    // default value is pulled from trails.settings.yml
    $max_to_display = \Drupal::config('trails.settings')->get('max_in_settings');

    // Add a select box of numbers form 1 to $max_to_display.
    $form['trails_block_num'] = array(
      '#type' => 'select',
      '#title' => t('Number of items to show'),
      '#default_value' => $this->configuration['num_to_show'] ?: 5,
      '#options' => array_combine(range(1, $max_to_display), range(1,$max_to_display)),
    );
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['num_to_show'] = $form_state->getValue('trails_block_num');
  }
}




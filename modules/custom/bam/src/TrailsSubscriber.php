<?php

/**
 * @file
 * Contains \Drupal\trails\TrailsSubscriber
 */

namespace Drupal\trails;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Subscribes to the kernel request event
 *
 * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
 *   The event to process.
 */
class TrailsSubscriber implements EventSubscriberInterface {
  /**
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The response event.
   */
  public function saveTrail(GetResponseEvent $event) {

  // \Drupal::state()->delete('trails.history');

    // Grab the trail history from a variable
    // $trail = variable_get('trails_history', array());
    $trail = \Drupal::state()->get('trails.history') ?:[];

    // Add current page to trail.
    $request = \Drupal::request();
    $route_match = \Drupal::routeMatch();
    

    $title = \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());
    // $title = !empty($title) ? $title : "(no title)";
    // $title = $title ?: "(no title)";
    // Returned $title can be string or object - we need to ensure $title is
    // a string otherwise subsequent code expecting a string may throw
    // an exception
    if (empty($title)) {
      $title = '(no title)';
    } elseif (!is_string($title)) {
      if (is_object($title) && $title instanceof TranslatableMarkup) {
        $title = $title->render();
      } else {
        $title = '(no title)';
      }
    }

    $current_url = Url::fromRoute('<current>');
    $path = $current_url->toString();

    $trail[] = array(
      'title' => $title,
      'path' => $path,
      'timestamp' => REQUEST_TIME,
    );

    // Save the trail as a variable
    // variable_set('trails_history', $trail);
    // if (count($trail) > 5) {
    //  $trail = array_slice($trail, -5, 5);
    // }
    \Drupal::state()->set('trails.history', $trail);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents(){
    $events[KernelEvents::REQUEST][] = array('saveTrail');
    return $events;
  }

}
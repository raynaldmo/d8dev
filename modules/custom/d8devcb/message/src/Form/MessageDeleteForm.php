<?php

namespace Drupal\message\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Message entities.
 *
 * @ingroup message
 */
class MessageDeleteForm extends ContentEntityDeleteForm {


}

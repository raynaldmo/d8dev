<?php

namespace Drupal\sample_block_a\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Copyright' block.
 *
 * @Block(
 *  id = "copyrighta",
 *  admin_label = @Translation("Copyrighta"),
 * )
 */
class Copyright extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['copyrighta']['#markup'] = 'Implement Copyright.';

    return $build;
  }

}

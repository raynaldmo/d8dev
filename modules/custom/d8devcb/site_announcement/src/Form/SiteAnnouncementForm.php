<?php

namespace Drupal\site_announcement\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SiteAnnouncementForm.
 *
 * @package Drupal\site_announcement\Form
 */
class SiteAnnouncementForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $site_announcement = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $site_announcement->label(),
      '#description' => $this->t("Label for the Site announcement."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $site_announcement->id(),
      '#machine_name' => [
        'exists' => '\Drupal\site_announcement\Entity\SiteAnnouncement::load',
      ],
      '#disabled' => !$site_announcement->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#required' => TRUE,
      '#default_value' => $site_announcement->getMessage(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $site_announcement = $this->entity;
    $status = $site_announcement->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Site announcement.', [
          '%label' => $site_announcement->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Site announcement.', [
          '%label' => $site_announcement->label(),
        ]));
    }
    $form_state->setRedirectUrl($site_announcement->toUrl('collection'));
  }

}

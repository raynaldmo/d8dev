<?php

namespace Drupal\site_announcement\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Site announcement entity.
 *
 * @ConfigEntityType(
 *   id = "site_announcement",
 *   label = @Translation("Site announcement"),
 *   handlers = {
 *     "list_builder" = "Drupal\site_announcement\SiteAnnouncementListBuilder",
 *     "form" = {
 *       "add" = "Drupal\site_announcement\Form\SiteAnnouncementForm",
 *       "edit" = "Drupal\site_announcement\Form\SiteAnnouncementForm",
 *       "delete" = "Drupal\site_announcement\Form\SiteAnnouncementDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_announcement\SiteAnnouncementHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "site_announcement",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/site_announcement/{site_announcement}",
 *     "add-form" = "/admin/structure/site_announcement/add",
 *     "edit-form" = "/admin/structure/site_announcement/{site_announcement}/edit",
 *     "delete-form" = "/admin/structure/site_announcement/{site_announcement}/delete",
 *     "collection" = "/admin/structure/site_announcement"
 *   },
 *   config_export = {
*      "id",
*      "label",
*      "message",
 *   }
 * )
 */
class SiteAnnouncement extends ConfigEntityBase implements SiteAnnouncementInterface {

  /**
   * The Site announcement ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Site announcement label.
   *
   * @var string
   */
  protected $label;

  /**
   * The announcement's message
   *
   * @var string
   */
  protected $message;

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->message;
  }
}

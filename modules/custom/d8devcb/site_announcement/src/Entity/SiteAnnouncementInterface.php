<?php

namespace Drupal\site_announcement\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Site announcement entities.
 */
interface SiteAnnouncementInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the message value
   *
   * @return string
   */
  public function getMessage();
}

<?php

namespace Drupal\sample_form\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SampleForm.
 *
 * @package Drupal\sample_form\Form
 */
class SampleForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sample_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sample_form.settings'];
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sample_form.settings');

    $form['company_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company Name'),
      '#default_value' => $config->get('company_name') ?: '',
      '#placeholder' => 'Enter Company Name',
      '#description' => $this->t('Company Name'),
      '#required' => TRUE,
    ];

    $form['phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone'),
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => $config->get('email') ?: '',
      '#required' => TRUE,
    ];

    $form['integer'] = [
      '#type' => 'number',
      '#title' => t('Number'),
      // The increment or decrement amount
      '#step' => 1,
      // Miminum allowed value
      '#min' => 0,
      // Maxmimum allowed value
      '#max' => 100,
    ];

    $form['date'] = [
      '#type' => 'date',
      '#title' => t('Date'),
      '#date_date_format' => 'Y-m-d',
    ];

    $form['website'] = [
      '#type' => 'url',
      '#title' => t('Website'),
    ];

    $form['search'] = [
      '#type' => 'search',
      '#title' => t('Search'),
      '#autocomplete_route_name' => FALSE,
    ];

    $form['range'] = [
      '#type' => 'range',
      '#title' => t('Range'),
      '#min' => 0,
      '#max' => 100,
      '#step' => 1,
    ];

    return parent::buildForm($form, $form_state);

  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->isValueEmpty('company_name')) {
      if (strlen($form_state->getValue('company_name')) <= 5) {
        // Set validation error.
        $form_state->setErrorByName('company_name', t('Company Name must be at least 5 characters longs'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /*
    foreach ($form_state->getValues() as $key => $value) {
    drupal_set_message($key . ': ' . $value);
    }
    */

    $config = $this->config('sample_form.settings');
    $company_name = $form_state->getValue('company_name');
    $email = $form_state->getValue('email');
    $config->set('company_name', $company_name);
    $config->set('email', $email);
  }

}

<?php

/**
 * @file
 * Contains message_type.page.inc.
 *
 * Page callback for Message type entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Message type templates.
 *
 * Default template: message_type.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_message_type(array &$variables) {
  // Fetch MessageType Entity Object.
  $message_type = $variables['elements']['#message_type'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

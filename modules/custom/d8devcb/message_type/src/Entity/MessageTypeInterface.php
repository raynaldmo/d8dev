<?php

namespace Drupal\message_type\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Message type entities.
 *
 * @ingroup message_type
 */
interface MessageTypeInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Message type type.
   *
   * @return string
   *   The Message type type.
   */
  public function getType();

  /**
   * Gets the Message type name.
   *
   * @return string
   *   Name of the Message type.
   */
  public function getName();

  /**
   * Sets the Message type name.
   *
   * @param string $name
   *   The Message type name.
   *
   * @return \Drupal\message_type\Entity\MessageTypeInterface
   *   The called Message type entity.
   */
  public function setName($name);

  /**
   * Gets the Message type creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Message type.
   */
  public function getCreatedTime();

  /**
   * Sets the Message type creation timestamp.
   *
   * @param int $timestamp
   *   The Message type creation timestamp.
   *
   * @return \Drupal\message_type\Entity\MessageTypeInterface
   *   The called Message type entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Message type published status indicator.
   *
   * Unpublished Message type are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Message type is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Message type.
   *
   * @param bool $published
   *   TRUE to set this Message type to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\message_type\Entity\MessageTypeInterface
   *   The called Message type entity.
   */
  public function setPublished($published);

}

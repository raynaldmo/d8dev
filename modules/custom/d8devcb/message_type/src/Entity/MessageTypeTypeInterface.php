<?php

namespace Drupal\message_type\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Message type type entities.
 */
interface MessageTypeTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}

<?php

namespace Drupal\message_type\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Message type type entity.
 *
 * @ConfigEntityType(
 *   id = "message_type_type",
 *   label = @Translation("Message type type"),
 *   handlers = {
 *     "list_builder" = "Drupal\message_type\MessageTypeTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\message_type\Form\MessageTypeTypeForm",
 *       "edit" = "Drupal\message_type\Form\MessageTypeTypeForm",
 *       "delete" = "Drupal\message_type\Form\MessageTypeTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\message_type\MessageTypeTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "message_type_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "message_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/message_type_type/{message_type_type}",
 *     "add-form" = "/admin/structure/message_type_type/add",
 *     "edit-form" = "/admin/structure/message_type_type/{message_type_type}/edit",
 *     "delete-form" = "/admin/structure/message_type_type/{message_type_type}/delete",
 *     "collection" = "/admin/structure/message_type_type"
 *   }
 * )
 */
class MessageTypeType extends ConfigEntityBundleBase implements MessageTypeTypeInterface {

  /**
   * The Message type type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Message type type label.
   *
   * @var string
   */
  protected $label;

}

<?php

namespace Drupal\message_type\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MessageTypeTypeForm.
 *
 * @package Drupal\message_type\Form
 */
class MessageTypeTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $message_type_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $message_type_type->label(),
      '#description' => $this->t("Label for the Message type type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $message_type_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\message_type\Entity\MessageTypeType::load',
      ],
      '#disabled' => !$message_type_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $message_type_type = $this->entity;
    $status = $message_type_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Message type type.', [
          '%label' => $message_type_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Message type type.', [
          '%label' => $message_type_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($message_type_type->toUrl('collection'));
  }

}

<?php

namespace Drupal\message_type\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Message type entities.
 *
 * @ingroup message_type
 */
class MessageTypeDeleteForm extends ContentEntityDeleteForm {


}

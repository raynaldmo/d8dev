<?php

namespace Drupal\message_type;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Message type entity.
 *
 * @see \Drupal\message_type\Entity\MessageType.
 */
class MessageTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\message_type\Entity\MessageTypeInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished message type entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published message type entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit message type entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete message type entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add message type entities');
  }

}

### Sample Field

11-11-17

* Module demonstrating how to create a custom field
* Taken from book " Drupal 8 Development Cookbook - Second Edition"
'Creating a custom field type'

#### Generate module and Field type boilerplate code
```
$drupal gm
$drupal gpf
```
* drupal gpf generates:  
src/ files  
Plugin/Field/FieldType/RealName.php  
Plugin/Field/FieldWidget/RealNameWidgetType.php  
Plugin/Field/FieldFormatter/RealNameFormatterType.php  

#### Customize Field Type
* RealName.php 
  * Modify schema() method to add first_name and last_name fields
    * [Schema API](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21database.api.php/group/schemaapi/8.4.x)
    documents how to define data types
  * Modify isEmpty() to check first_name and last_name
  
#### Customize Field Widget
* RealNameWidgetType.php
  * Change widget id to real_name_default
  * Update formElement() method by adding first_name and last_name elements
  
#### Customize Field Formatter
* RealNameFormatterType.php
  * Update viewElements() method to display first and last name in a single line     

<?php

namespace Drupal\sample_module\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\sample_module\Routing
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // change path of sample_module.sample_page
    if ($route = $collection->get('sample_module.sample_page')) {
      // $route->setPath('/sample-module/sample-custom-page');
    }
  }

}

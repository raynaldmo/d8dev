<?php

namespace Drupal\sample_module\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class SamplePageController.
 *
 * @package Drupal\sample_module\Controller
 */
class SamplePageController extends ControllerBase {

  /**
   * Sample Custom page.
   *
   * @return array
   *   Return page markup as render array
   */
  public function samplePage() {
    $build = array();
    $items = array();

    $build['markup'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<strong><em>Welcome to my sample custom page</em></strong>'),
    ];

    $build['simple_extras'] = [
      '#markup' => '<p>' . $this->t('Text wrapped with blockquote tag.') . '</p>',
      '#prefix' => '<blockquote>',
      '#suffix' => '</blockquote>',
    ];

    $build['list'] = [
      '#theme' => 'item_list',
      '#title' => $this->t('A list of items'),
      '#items' => [
        $this->t('List item #1'),
        $this->t('List item #2'),
      ],
    ];

    $build['table'] = [
      '#type' => 'table',
      '#caption' => $this->t('Our favorite colors.'),
      '#header' => [$this->t('Name'), $this->t('Favorite color')],
      '#rows' => [
        [$this->t('Ashley'), $this->t('<span style="color:teal">teal</span>')],
        [$this->t('Addi'), $this->t('<span style="color:green">green</span>')],
        [$this->t('Blake'), $this->t('<span style="color:darkred">darkred</span>')],
        [$this->t('Enid'), $this->t('<span style="color:indigo">indigo</span>')],
        [$this->t('Jamaal'), $this->t('<span style="color:orange">orange</span>')],
      ],
      '#description' => $this->t('Example of using #type.'),
    ];

    $build['marquee'] = [
      '#theme' => 'sample_module_marquee',
      '#content' => $this->t('This is marquee text'),
      '#attributes' => [
        'class' => ['my-marquee-element'],
        'direction' => 'right',
      ],
    ];

    return $build;
  }

  public function samplePageParam($name) {
    return [
      '#markup' => t('My name is %name', ['%name' => $name]),
    ];
  }

}

Very simple demo of behat test for Drupal

From this directory:

$vendor/bin/behat
Feature: Home Page
  Tests the home page

  Scenario: View home page                   # features/homepage.feature:4
    Given I am on "/"                        # Drupal\DrupalExtension\Context\MinkContext::visit()
    Then I should see "Welcome to d8dev.dev" # Drupal\DrupalExtension\Context\MinkContext::assertPageContainsText()

1 scenario (1 passed)
2 steps (2 passed)
0m0.30s (12.33Mb)
